﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DTO
{
    public class RoleDTO
    {
        public int sysid { get; set; }
        public string name { get; set; }        
    }
}
