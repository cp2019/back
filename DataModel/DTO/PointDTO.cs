﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DTO
{
    public class PointDTO
    {
        public int sysid { get; set; }
        public int lat { get; set; }
        public int lon { get; set; }
    }
}
