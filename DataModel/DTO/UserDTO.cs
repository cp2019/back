﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DTO
{
    public class UserDTO
    {
        public int sysid { get; set; }
        public string login { get; set; }
        public string passwd { get; set; }
        public string token { get; set; }
        public string name { get; set; }
        public string patronymic { get; set; }
        public string surname { get; set; }
        public string photoUrl { get; set; }
        public string birthday { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public int doneRevisionCount { get; set; }
        public int defectRevisionCount { get; set; }
        public RoleDTO role { get; set; }        
    }
}
