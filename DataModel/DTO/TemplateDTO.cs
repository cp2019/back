﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DTO
{
    public class TemplateDTO
    {
        public int sysid { get; set; }
        public string name { get; set; }
        public List<ElementDTO> elemenstDTO { get; set; }
        public TemplateDTO()
        {
            elemenstDTO = new List<ElementDTO>();
        }
    }
    public class TemplateDTODesirialize
    {
        public int sysid { get; set; }
        public string name { get; set; }
        public List<ElementDTODesirialize> elements { get; set; }
        public TemplateDTODesirialize()
        {
            elements = new List<ElementDTODesirialize>();
        }
    }

}
