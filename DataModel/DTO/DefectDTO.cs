﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DTO
{
    public class DefectDTO
    {
        public int sysid { get; set; }
        public string name { get; set; }
        public int defectCategoryId { get; set; }
        public int defectLevelId { get; set; }
    }
}
