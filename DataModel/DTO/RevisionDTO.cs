﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DTO
{
    public class RevisionDTO : StatusDTO
    {
        public string description { get; set; }
        public int assigneeUserId { get; set; }
        public string timeSpent { get; set; }
        public string startDateTime { get; set; }
        public string endDateTime { get; set; }
        public int statusId { get; set; }
        public List<CommentDTO> commentsIds { get; set; }
        public List<DefectDTO> defectIds { get; set; }
        public RevisionDTO()
        {
            commentsIds = new List<CommentDTO>();
            defectIds = new List<DefectDTO>();
        }

    }
}
