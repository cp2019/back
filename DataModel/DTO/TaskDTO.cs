﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DTO
{
    public class TaskDTO
    {
        public int sysid { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string sessionTask { get; set; }

        public List<TemplateDTO> templatesDTO { get; set; }
    }
}
