﻿using DataModel.AdminData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DTO
{
    public class FileDTO
    {
        public int sysid { get; set; }
        public string name { get; set; }
        public string data { get; set; }
        public ElementTypeDTO filetype { get; set; }
    }
}
