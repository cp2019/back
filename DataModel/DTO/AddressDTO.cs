﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DTO
{
    public class AddressDTO
    {
        public int sysid { get; set; }
        public string street { get; set; }
        public string city { get; set; }
        public string house { get; set; }
        public int lat { get; set; }
        public int lon { get; set; }
    }
}
