﻿using DataModel.AdminData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DTO
{
    public class ElementDTO
    {
        public int sysid { get; set; }
        public int step { get; set; }
        public string id { get; set; }
        public string text { get; set; }
        public ElementTypeDTO elementType { get; set; }
    }
    public class ElementDTODesirialize
    {
        public int step { get; set; }
        public int type { get; set; }
        public string id { get; set; }
        public string text { get; set; }
    }

}
