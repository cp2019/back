﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DTO
{
    public class CommentDTO
    {
        public int sysid { get; set; }
        public string text { get; set; }
        public string timeCreated { get; set; }
        public string timeLastUpdate { get; set; }
        public string address { get; set; }
        public UserDTO user { get; set; }
    }
}
