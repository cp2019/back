﻿using DataModel.DTO;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.AdminData
{
    public class h_file : XpSinhObject
    {
        private int _sysid;
        private string _name;
        private string _data;
        private h_elementType _h_Filetype;
        public h_file(Session session)
            : base(session)
        {

        }
        [Key(true)]
        public int Sysid
        {
            get { return _sysid; }
            set { SetPropertyValue<int>("Sysid", ref _sysid, value); }
        }
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue<string>("Name", ref _name, value); }
        }
        public string Data
        {
            get { return _data; }
            set { SetPropertyValue<string>("Data", ref _data, value); }
        }
        public h_elementType Filetype
        {
            get { return _h_Filetype; }
            set { SetPropertyValue<h_elementType>("Filetype", ref _h_Filetype, value); }
        }
        public FileDTO GetFileDTO()
        {
            var fileDTO = new FileDTO();
            fileDTO.sysid = Sysid;
            fileDTO.name = Name;
            fileDTO.data = Data;
            fileDTO.filetype = Filetype.GetFileTypeDTO();
            return fileDTO;
        }

    }
}
