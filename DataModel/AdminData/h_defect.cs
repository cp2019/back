﻿using DataModel.DTO;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.AdminData
{
    public class h_defect : XpSinhObject
    {
        private int _sysid;
        private string _name;
        private int _defectCategoryId;
        private int _defectLevelId;
        private h_revision _revison;

        public h_defect(Session session)
            : base(session)
        {

        }

        [Key(true)]
        public int Sysid
        {
            get { return _sysid; }
            set { SetPropertyValue<int>("Sysid", ref _sysid, value); }
        }
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue<string>("Text", ref _name, value); }
        }
        public int DefectCategoryId
        {
            get { return _defectCategoryId; }
            set { SetPropertyValue<int>("DefectCategoryId", ref _defectCategoryId, value); }
        }
        public int DefectLevelId
        {
            get { return _defectLevelId; }
            set { SetPropertyValue<int>("TimeLastUpdate", ref _defectLevelId, value); }
        }
        [Association("Revision-defectsIds")]
        public h_revision Revision
        {
            get { return _revison; }
            set { SetPropertyValue<h_revision>("Revision", ref _revison, value); }
        }
        public DefectDTO GetDefectDTO()
        {
            var defectDto = new DefectDTO();
            defectDto.sysid = Sysid;
            defectDto.name = Name;
            defectDto.defectCategoryId = DefectCategoryId;
            defectDto.defectLevelId = DefectLevelId;
            return defectDto;
        }
    }
}
