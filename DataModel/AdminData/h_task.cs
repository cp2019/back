﻿using DataModel.DTO;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.AdminData
{
    public class h_task : XpSinhObject
    {
        private int _sysid;
        private string _name;
        private string _description;
        private string _sessionTask;
        private h_user _user;
        private XPCollection<h_template> _templates;

        public h_task(Session session)
            : base(session)
        {
            _templates = new XPCollection<h_template>(session);
        }
        [Key(true)]
        public int Sysid
        {
            get { return _sysid; }
            set { SetPropertyValue<int>("Sysid", ref _sysid, value); }
        }
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue<string>("Name", ref _name, value); }
        }
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue<string>("Description", ref _description, value); }
        }
        public string SessionTask
        {
            get { return _sessionTask; }
            set { SetPropertyValue<string>("SessionTask", ref _sessionTask, value); }
        }
        [NonPersistent]
        public XPCollection<h_template> Templates
        {
            get
            {
                return _templates;
            }
            set
            {
                { SetPropertyValue<XPCollection<h_template>>("Templates", ref _templates, value); }
            }
        }
        public h_user User
        {
            get { return _user; }
            set { SetPropertyValue<h_user>("User", ref _user, value); }
        }
        public TaskDTO GetTaskDTO(h_task task)
        {
            TaskDTO taskDTO = new TaskDTO();
            taskDTO.description = task.Description;
            taskDTO.name = task.Name;
            taskDTO.sessionTask = task.SessionTask;
            taskDTO.sysid = task.Sysid;
            foreach (var item in Templates)
                taskDTO.templatesDTO.Add(item.GetTemplateDTO(item));
            return taskDTO;
        }


    }
}
