﻿using DataModel.DTO;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.AdminData
{
    public class h_address : XpSinhObject
    {
        private int _sysid;
        private string _street;
        private string _city;
        private string _house;
        private int _lat;
        private int _lon;
        public h_address(Session session)
            : base(session)
        {

        }
        [Key(true)]
        public int Sysid
        {
            get { return _sysid; }
            set { SetPropertyValue<int>("Sysid", ref _sysid, value); }
        }
        public string City
        {
            get { return _city; }
            set { SetPropertyValue<string>("City", ref _city, value); }
        }
        public string Street
        {
            get { return _street; }
            set { SetPropertyValue<string>("Street", ref _street, value); }
        }
        public string House
        {
            get { return _house; }
            set { SetPropertyValue<string>("House", ref _house, value); }
        }
        public int Lon
        {
            get { return _lon; }
            set { SetPropertyValue<int>("Lon", ref _lon, value); }
        }
        public int Lat
        {
            get { return _lat; }
            set { SetPropertyValue<int>("Lat", ref _lat, value); }
        }
        public AddressDTO GetAddressDTO()
        {
            var addressDTO = new AddressDTO();
            addressDTO.sysid = Sysid;
            addressDTO.street = Street;
            addressDTO.city = City;
            addressDTO.house = House;
            addressDTO.lat = Lat;
            addressDTO.lon = Lon;
            return addressDTO;
        }
    }
}
