﻿using DataModel.DTO;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.AdminData
{
    public class h_elemnet : XpSinhObject
    {
        private int _sysid;
        private int _step;
        private string _id;
        private string _text;
        private h_template _template;
        private h_elementType _elementType;
        public h_elemnet(Session session)
            : base(session)
        {

        }
        [Key(true)]
        public int Sysid
        {
            get { return _sysid; }
            set { SetPropertyValue<int>("Sysid", ref _sysid, value); }
        }
        public int Step
        {
            get { return _step; }
            set { SetPropertyValue<int>("Sysid", ref _step, value); }
        }
        public string Id
        {
            get { return _id; }
            set { SetPropertyValue<string>("Name", ref _id, value); }
        }
        public string Text
        {
            get { return _text; }
            set { SetPropertyValue<string>("Text", ref _text, value); }
        }
        public h_elementType ElementType
        {
            get { return _elementType; }
            set { SetPropertyValue<h_elementType>("ElementType", ref _elementType, value); }
        }

        [Association("Template-Elements")]
        public h_template Template
        {
            get { return _template; }
            set { SetPropertyValue<h_template>("Template", ref _template, value); }
        }
        public ElementDTO GetElementDTO(h_elemnet element)
        {
            ElementDTO elementDTO = new ElementDTO();
            elementDTO.sysid = element.Sysid;
            elementDTO.id = element.Id;
            elementDTO.text = element.Text;
            elementDTO.step = element.Step;
            elementDTO.elementType = element.ElementType.GetFileTypeDTO();
            return elementDTO;

        }


    }
}
