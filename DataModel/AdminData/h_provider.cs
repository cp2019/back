﻿using DataModel.DTO;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.AdminData
{
    public class h_provider : XpSinhObject
    {
        private int _sysid;
        private string _name;
        public h_provider(Session session)
            : base(session)
        {

        }
        [Key(true)]
        public int Sysid
        {
            get { return _sysid; }
            set { SetPropertyValue<int>("Sysid", ref _sysid, value); }
        }
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue<string>("Name", ref _name, value); }
        }
        public ProviderDTO GetProviderDTO()
        {
            var providerDTO = new ProviderDTO();
            providerDTO.sysid = Sysid;
            providerDTO.name = Name;
            return providerDTO;
        }
    }
}
