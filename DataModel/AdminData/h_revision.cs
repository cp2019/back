﻿using DataModel.DTO;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.AdminData
{
    public class h_revision : XpSinhObject
    {
        private int _sysid;
        private string _name;
        private string _description;
        private int _assigneeUserId;
        private string _timeSpent;
        private string _startDateTime;
        private string _endDateTime;
        private int _statusId;
        public h_revision(Session session)
            : base(session)
        {

        }
        [Key(true)]
        public int Sysid
        {
            get { return _sysid; }
            set { SetPropertyValue<int>("Sysid", ref _sysid, value); }
        }
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue<string>("Name", ref _name, value); }
        }
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue<string>("Description", ref _description, value); }
        }
        public int AssigneeUserId
        {
            get { return _assigneeUserId; }
            set { SetPropertyValue<int>("AssigneeUserId", ref _assigneeUserId, value); }
        }
        public string TimeSpent
        {
            get { return _timeSpent; }
            set { SetPropertyValue<string>("TimeSpent", ref _timeSpent, value); }
        }
        public string StartDateTime
        {
            get { return _startDateTime; }
            set { SetPropertyValue<string>("StartDateTime", ref _startDateTime, value); }
        }
        public string EndDateTime
        {
            get { return _endDateTime; }
            set { SetPropertyValue<string>("EndDateTime", ref _endDateTime, value); }
        }
        public int StatusId
        {
            get { return _statusId; }
            set { SetPropertyValue<int>("StatusId", ref _statusId, value); }
        }
        [Association("Revision-defectsIds")]
        public XPCollection<h_defect> defectsIds
        {
            get
            {
                return GetCollection<h_defect>("defectsIds");
            }
        }
        [Association("Revision-commentsIds")]
        public XPCollection<h_comment> commentsIds
        {
            get
            {
                return GetCollection<h_comment>("commentsIds");
            }
        }
        public RevisionDTO GetRevisionDTO()
        {
            var revisionDTO = new RevisionDTO();
            revisionDTO.sysid = Sysid;
            revisionDTO.name = Name;
            revisionDTO.description = Description;
            revisionDTO.assigneeUserId = AssigneeUserId;
            revisionDTO.endDateTime = EndDateTime;
            revisionDTO.statusId = StatusId;
            revisionDTO.timeSpent = TimeSpent;
            revisionDTO.startDateTime = StartDateTime;
            foreach (var defect in defectsIds)
                revisionDTO.defectIds.Add(defect.GetDefectDTO());
            foreach (var comment in commentsIds)
                revisionDTO.commentsIds.Add(comment.GetCommentDTO());
            return revisionDTO;
        }
    }
}
