﻿using DataModel.DTO;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.AdminData
{
    public class h_point : XpSinhObject
    {
        private int _sysid;
        private int _lat;
        private int _lon;
        public h_point(Session session)
            : base(session)
        {

        }
        [Key(true)]
        public int Sysid
        {
            get { return _sysid; }
            set { SetPropertyValue<int>("Sysid", ref _sysid, value); }
        }
        public int Lon
        {
            get { return _lon; }
            set { SetPropertyValue<int>("Lon", ref _lon, value); }
        }
        public int Lat
        {
            get { return _lat; }
            set { SetPropertyValue<int>("Lat", ref _lat, value); }
        }
        public PointDTO GetAddressDTO()
        {
            var pointDTO = new PointDTO();
            pointDTO.sysid = Sysid;
            pointDTO.lat = Lat;
            pointDTO.lon = Lon;
            return pointDTO;
        }
    }
}
