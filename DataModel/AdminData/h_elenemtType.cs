﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.DTO;
using DevExpress.Xpo;

namespace DataModel.AdminData
{
    public class h_elementType : XpSinhObject
    {
        private string _discription;
        private int _sysid;
        private string _name;
        public h_elementType(Session session) : base(session)
        {
        }
        [Key(true)]
        public int Sysid
        {
            get { return _sysid; }
            set { SetPropertyValue<int>("Sysid", ref _sysid, value); }
        }
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue<string>("Name", ref _name, value); }
        }
        public string Discription
        {
            get { return _discription; }
            set { SetPropertyValue<string>("Discription", ref _discription, value); }
        }
        public ElementTypeDTO GetFileTypeDTO()
        {
            var fileTypeDTO = new ElementTypeDTO();
            fileTypeDTO.sysid = Sysid;
            fileTypeDTO.discription = Discription;
            fileTypeDTO.name = Name;
            return fileTypeDTO;
        }
    }
}
