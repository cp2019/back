﻿using DataModel.DTO;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.AdminData
{
    public class h_taskcategory : XpSinhObject
    {
        private int _sysid;
        private string _discription;
        public h_taskcategory(Session session)
            : base(session)
        {

        }
        [Key(true)]
        public int Sysid
        {
            get { return _sysid; }
            set { SetPropertyValue<int>("Sysid", ref _sysid, value); }
        }
        public string Discription
        {
            get { return _discription; }
            set { SetPropertyValue<string>("Discription", ref _discription, value); }
        }
        public TaskCategoryDTO GetTaskCategoryDTO()
        {
            var taskCategoryDTO = new TaskCategoryDTO();
            taskCategoryDTO.sysid = Sysid;
            taskCategoryDTO.discription = Discription;
            return taskCategoryDTO;
        }
    }
}
