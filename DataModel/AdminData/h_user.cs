﻿using DataModel.DTO;
using DevExpress.Xpo;
using System;
using System.Web.Script.Serialization;

namespace DataModel.AdminData
{
    public class h_user : XPLiteObject
    {
        private int _sysid;
        private string _login;
        private string _passwd;
        private string _token;
        private string _name;
        private string _patronymic;
        private string _surname;
        private string _photoUrl;
        private string _birthday;
        private string _phone;
        private string _email;
        private int _doneRevisionCount;
        private int _defectRevisionCount;
        private h_role _role;
        public h_user(Session session)
            : base(session)
        {

        }
        [Key(true)]
        public int Sysid
        {
            get { return _sysid; }
            set { SetPropertyValue<int>("Sysid", ref _sysid, value); }
        }
        ////  public h_role Role
        //  {
        //      get { return _role; }
        //      set { SetPropertyValue<h_role>("Role", ref _role, value); }
        //  }
        public string Login
        {
            get { return _login; }
            set { SetPropertyValue<string>("Login", ref _login, value); }
        }
        public string Passwd
        {
            get { return _passwd; }
            set { SetPropertyValue<string>("Passwd", ref _passwd, value); }
        }
        public string Token
        {
            get { return _token; }
            set { SetPropertyValue<string>("Token", ref _token, value); }
        }
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue<string>("Name", ref _name, value); }
        }
        public string Patronymic
        {
            get { return _patronymic; }
            set { SetPropertyValue<string>("Name", ref _patronymic, value); }
        }
        public string Surname
        {
            get { return _surname; }
            set { SetPropertyValue<string>("Surname", ref _surname, value); }
        }
        public string PhotoUrl
        {
            get { return _photoUrl; }
            set { SetPropertyValue<string>("PhotoUrl", ref _photoUrl, value); }
        }
        public string Birthday
        {
            get { return _birthday; }
            set { SetPropertyValue<string>("Birthday", ref _birthday, value); }
        }
        public string Phone
        {
            get { return _phone; }
            set { SetPropertyValue<string>("Phone", ref _phone, value); }
        }
        public string Email
        {
            get { return _email; }
            set { SetPropertyValue<string>("Email", ref _email, value); }
        }
        public int DoneRevisionCount
        {
            get { return _doneRevisionCount; }
            set { SetPropertyValue<int>("DoneRevisionCount", ref _doneRevisionCount, value); }
        }
        public int DefectRevisionCount
        {
            get { return _defectRevisionCount; }
            set { SetPropertyValue<int>("DefectRevisionCount", ref _defectRevisionCount, value); }
        }
        public h_role Role
        {
            get { return _role; }
            set { SetPropertyValue<h_role>("Role", ref _role, value); }
        }

        public UserDTO GetUserDTO()
        {
            var userDTO = new UserDTO();
            userDTO.sysid = Sysid;
            userDTO.login = Login;
            userDTO.passwd = Passwd;
            userDTO.token = Token;
            userDTO.name = Name;
            userDTO.patronymic = Patronymic;
            userDTO.phone = Phone;
            userDTO.photoUrl = PhotoUrl;
            userDTO.surname = Surname;
            userDTO.birthday = Birthday;
            userDTO.email = Email;
            userDTO.doneRevisionCount = DoneRevisionCount;
            userDTO.defectRevisionCount = DefectRevisionCount;
            userDTO.role = Role.GetRoleDTO();
            return userDTO;
        }
    }


}
