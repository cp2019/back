﻿using DataModel.DTO;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.AdminData
{
    public class h_comment : XPLiteObject
    {
        private int _sysid;
        private string _text;
        private string _timeCreated;
        private string _timeLastUpdate;
        private string _address;
        private h_user _h_User;
        private h_revision _revision;
        
        public h_comment(Session session)
            : base(session)
        {

        }

        [Key(true)]
        public int Sysid
        {
            get { return _sysid; }
            set { SetPropertyValue<int>("Sysid", ref _sysid, value); }
        }
        public string Text
        {
            get { return _text; }
            set { SetPropertyValue<string>("Text", ref _text, value); }
        }
        public string TimeCreated
        {
            get { return _timeCreated; }
            set { SetPropertyValue<string>("TimeCreated", ref _timeCreated, value); }
        }
        public string TimeLastUpdate
        {
            get { return _timeLastUpdate; }
            set { SetPropertyValue<string>("TimeLastUpdate", ref _timeLastUpdate, value); }
        }
        public string Address
        {
            get { return _address; }
            set { SetPropertyValue<string>("Address", ref _address, value); }
        }
        public h_user User
        {
            get { return _h_User; }
            set { SetPropertyValue<h_user>("User", ref _h_User, value); }
        }
        [Association("Revision-commentsIds")]
        public h_revision Revision
        {
            get { return _revision; }
            set { SetPropertyValue<h_revision>("Revision", ref _revision, value); }
        }
        public CommentDTO GetCommentDTO()
        {
            var commentDTO = new CommentDTO();
            commentDTO.sysid = Sysid;
            commentDTO.text = Text;
            commentDTO.timeCreated = TimeCreated;
            commentDTO.timeLastUpdate = TimeLastUpdate;
            commentDTO.address = Address;
            commentDTO.user = User.GetUserDTO();
            return commentDTO;
        }
    }
}
