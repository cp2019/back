﻿using DataModel.DTO;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.AdminData
{
    public class h_template : XpSinhObject
    {
        private int _sysid;
        private string _name;
        public h_template(Session session)
            : base(session)
        {

        }
        [Key(true)]
        public int Sysid
        {
            get { return _sysid; }
            set { SetPropertyValue<int>("Sysid", ref _sysid, value); }
        }
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue<string>("Name", ref _name, value); }
        }
        [Association("Template-Elements")]
        public XPCollection<h_elemnet> Elements
        {
            get
            {
                return GetCollection<h_elemnet>("Elements");
            }
        }
        public TemplateDTO GetTemplateDTO(h_template templateSource)
        {
            TemplateDTO template = new TemplateDTO();
            template.name = templateSource.Name;
            template.sysid = templateSource.Sysid;
            foreach (var item in Elements)
                template.elemenstDTO.Add(item.GetElementDTO(item));
            return template;
        }

    }
}
