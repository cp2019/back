﻿using DataModel.DTO;
using DevExpress.Xpo;

namespace DataModel.AdminData
{
    public class h_role : XpSinhObject
    {
        private int _sysid;
        private string _name;
        public h_role(Session session)
            : base(session)
        {

        }
        [Key(true)]
        public int Sysid
        {
            get { return _sysid; }
            set { SetPropertyValue<int>("Sysid", ref _sysid, value); }
        }
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue<string>("Name", ref _name, value); }
        }
        public RoleDTO GetRoleDTO()
        {
            var roleDTO = new RoleDTO();
            roleDTO.sysid = Sysid;
            roleDTO.name = Name;
            return roleDTO;
        }

    }
}
