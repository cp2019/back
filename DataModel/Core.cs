﻿using DataModel.TransportData;
using DevExpress.Xpo;
using DevExpress.Xpo.DB.Helpers;
using DevExpress.Xpo.Helpers;
using DevExpress.Xpo.Metadata;
using DevExpress.Xpo.Metadata.Helpers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    public class Core
    {

        public static object GetObjectType(string boTypeShortName, string nameDll, string pathController,
            params object[] dops)
        {
            Type boType = GetType(boTypeShortName, nameDll);
            if (boType == null)
                return null;
            ConstructorInfo ctor = boType.GetConstructor(new Type[] { });
            return ctor.Invoke(dops); //Activator.CreateInstance(boType, dops);
        }

        public static object GetObjectType(Type typeClass, params object[] dops)
        {
            if (dops != null && dops.Length > 0)
            {
                if (typeClass.GetConstructor(new Type[] { dops[0].GetType() }) == null)
                    dops = null;
            }
            return Activator.CreateInstance(typeClass, dops);
        }

        public static Type GetType(string boTypeShortName, string nameDll)
        {
            AppDomain currentDomain = AppDomain.CurrentDomain;
            try
            {
                currentDomain.AssemblyResolve += MyResolveEventHandler;
                string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, nameDll);
                Assembly asm = Assembly.LoadFile(path);
                var boType =
                    asm.GetTypes()
                        .FirstOrDefault(
                            f => f.Name.ToLower().Replace("_", "").Equals(boTypeShortName.ToLower().Replace("_", "")));
                return boType;
            }
            finally
            {
                currentDomain.AssemblyResolve -= MyResolveEventHandler;
            }
            /*Assembly assembly = Assembly.LoadFrom(AppDomain.CurrentDomain.BaseDirectory + "\\" + nameDll);
            //if (nameDll.ToLower() == "frgo.ncore.dll") ;
            //    assembly.LoadModule(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Frgo.DocumentBuilder.dll"), )
            Type t = ass.GetTypes().FirstOrDefault(f => f.Name.ToLower().Replace("_", "").Equals(boTypeShortName.ToLower().Replace("_", "")));
            //string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, nameDll);
            //Assembly asm = Assembly.LoadFrom(path);
            //var boType = asm.GetTypes().FirstOrDefault(f => f.Name.ToLower().Replace("_", "").Equals(boTypeShortName.ToLower().Replace("_", "")));
            //return boType;
            return t;*/
        }

        private static Assembly MyResolveEventHandler(object sender, ResolveEventArgs args)
        {
            string dllName = args.Name.Split(new[] { ',' })[0] + ".dll";
            return Assembly.LoadFrom(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, dllName));
        }

        public static object ExecuteControllerMethod(RequestData p, string nameDll = "Monica.Proxy.exe",
            string pathController = "Monica.Proxy.Controllers")
        {
            try
            {
                var sw = new Stopwatch();
                sw.Start();


                object bo = GetObjectType(p.Controller, nameDll, pathController) ??
                            CreateInstanceByNameControllersClass(p.Controller);
                var metodInit = bo.GetType().GetMethod("InitController");
                var metodClose = bo.GetType().GetMethod("ClosedController");
                Console.WriteLine($"*TIME* CoreExecute Before Invoke Method = {p.Command} >> : {sw.Elapsed.TotalMilliseconds}");
                if (metodInit != null && p.Command != "Authorization")
                    metodInit.Invoke(bo, new object[] { p });
                var invokeRes = bo.GetType()
                    .InvokeMember(p.Command, BindingFlags.InvokeMethod, null, bo, new object[] { p.ClassParams });
                if (metodClose != null)
                    metodClose.Invoke(bo, null);
                var mySession = bo.GetType().GetProperty("MySession");
                Console.WriteLine($"*TIME* CoreExecute After Invoke Method decision >> : {sw.Elapsed.TotalMilliseconds}");
                if (mySession != null && ((UnitOfWork)mySession.GetValue(bo, null)) != null)
                {
                    var session = ((UnitOfWork)mySession.GetValue(bo, null));
                    session.Disconnect();
                    session.Dispose();
                }
                Console.WriteLine($"*TIME* CoreExecute After Save session and Disconnect >> : {sw.Elapsed.TotalMilliseconds}");
                return invokeRes;
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }

        }

        /// <summary>
        /// Метод поиска и инициализации классов по заданному имени
        /// </summary>
        /// <param name="fullTypeName">Тип, который необходимо инициальзировать</param>
        /// <param name ="parametr">Параметр, который передается в конструктор инициализации</param>
        /// <returns>Имя класса. Например, Frgo.Admin.Setting.SolutionForm</returns>
        public static object CreateInstance(string fullTypeName, params object[] parametr)
        {
            IEnumerable<Type> types = from assembly in AppDomain.CurrentDomain.GetAssemblies()
                                      let type = assembly.GetType(fullTypeName)
                                      where type != null && assembly.FullName.Split(',')[0].StartsWith("Frgo.")
                                      select type;

            var enumerable = types as IList<Type> ?? types.ToList();
            if (enumerable.Any())
                return Activator.CreateInstance(enumerable.First(), parametr);
            throw new ApplicationException(string.Format("Type \"{0}\" not exists in executing assemblies", fullTypeName));
        }

        public static Type GetTypeByName(string nameClass)
        {
            var str = "";
            try
            {
                var type = GetTypeByName(nameClass, "frgo.clientmodel");
                str = "frgo.clientmodel";
                type = type ?? GetTypeByName(nameClass, "frgo.clientprovider");
                str = "frgo.clientprovider";
                type = type ?? GetTypeByName(nameClass, "monica");
                str = "monica";
                type = type ?? GetTypeByName(nameClass, "corecontrollers");
                str = "corecontrollers";
                return type;
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw new Exception(nameClass + "|" + str);
            }
        }

        private static Type GetTypeByName(string nameClass, string project)
        {
            IEnumerable<Type> types = from assembly in AppDomain.CurrentDomain.GetAssemblies().Where(a =>
            {
                var aa = a.FullName.Split(',')[0];
                return aa.ToLower().StartsWith(project) && !aa.ToLower().StartsWith("frgo.admin");
            })
                                      let type = assembly.GetTypes().FirstOrDefault(ass => (typeof(XPBaseObject).IsAssignableFrom(ass)) && ass.Name.Equals(nameClass))
                                      where type != null
                                      select type;

            var enumerable = types as IList<Type> ?? types.ToList();
            if (enumerable.Any())
                return enumerable.FirstOrDefault();
            return null;
        }


        public static object CreateInstanceByNameControllersClass(string nameClass, params object[] parametr)
        {
            IEnumerable<Type> types = from assembly in AppDomain.CurrentDomain.GetAssemblies().Where(a => a.FullName.Split(',')[0].StartsWith("Monica."))
                                      let type = assembly.GetTypes().FirstOrDefault(ass => ass.FullName.EndsWith(nameClass))
                                      where type != null
                                      select type;

            var enumerable = types as IList<Type> ?? types.ToList();
            if (enumerable.Any())
                return Activator.CreateInstance(enumerable.First(), parametr);
            throw new ApplicationException(string.Format("Type \"{0}\" not exists in executing assemblies", nameClass));
        }

        public static bool ReferenceType(Type type, Type typeReference)
        {
            if (type == typeReference)
                return true;
            if (type == null)
                return false;

            return ReferenceType(type.BaseType, typeReference);
        }

        public static Type FindType(string collectionModelName)
        {
            Assembly[] assemblys = AppDomain.CurrentDomain.GetAssemblies();
            // Обявление переменной для определения типа класса
            Type typeModel = null;
            //Цикл по всем сборкам до тех пор пока не наступит конец коллекции сборок
            foreach (Assembly current in assemblys)
            {
                //Если имя сборки равно имени свойтсву Assembly, тогда...
                if (collectionModelName.StartsWith(current.GetName().ToString().Split(',')[0]))
                    //Присваиваем значение переменной типа класса из сборки, которая указана в  свойстве Assembly.
                    //Если сборку не удалось найти, тогда переменной будет присвоено значение "null"
                    typeModel = current.GetType(collectionModelName);
            }
            return typeModel;
        }

        public static void RunDllFromPath(string mask = "*.*.dll")
        {
            var ssDll = Directory.GetFiles(Environment.CurrentDirectory, mask).ToList();
            string text = Environment.CurrentDirectory;
            for (int i = ssDll.Count - 1; i >= 0; i--)
            {
                AppDomain.CurrentDomain.Load(ssDll[i].Replace(string.Format("{0}\\", text), "").Replace(".dll", ""));
            }
        }
    }

    public class ConvertCollectionInDataTable
    {
        private readonly XPBaseCollection _collection;



        public ConvertCollectionInDataTable(XPBaseCollection collection)
        {
            _collection = collection;
        }

        public DataTable GetTableForCollection()
        {
            var dt = CreateStructTable();
            foreach (XPBaseObject model in _collection)
                dt.Rows.Add(CreateDataRow(dt, model));
            return dt;
        }

        private DataRow CreateDataRow(DataTable dt, XPBaseObject model)
        {
            var row = dt.NewRow();
            foreach (XPMemberInfo memberInfo in model.ClassInfo.PersistentProperties)
                row[memberInfo.Name] = model.GetMemberValue(memberInfo.Name);
            return row;
        }

        private DataTable CreateStructTable()
        {
            var dt = new DataTable();
            foreach (XPMemberInfo memberInfo in _collection.GetObjectClassInfo().PersistentProperties)
                dt.Columns.Add(memberInfo.Name, memberInfo.MemberType);
            return dt;
        }
    }

    public class MainXmlAttribute : Attribute
    {

    }

    public class NotLoadByXmlAttribute : Attribute
    {

    }

    public class NotLoadPropByXmlAttribute : Attribute
    {

    }

    public class MainXmlCollectionAttribute : Attribute
    {
        private Type _typeCollection;

        public MainXmlCollectionAttribute(Type typeCollection)
        {
            _typeCollection = typeCollection;
        }

        public MainXmlCollectionAttribute()
        {
        }

        public Type TypeCollection
        {
            get { return _typeCollection; }
        }
    }

    public class AssociationCommectionAttribute : Attribute
    {
        private Type _typeCollection;

        public AssociationCommectionAttribute(Type typeCollection)
        {
            _typeCollection = typeCollection;
        }

        public AssociationCommectionAttribute()
        {
        }

        public Type TypeCollection
        {
            get { return _typeCollection; }
        }
    }

    public class WarnigException : Exception
    {
        private readonly string _typeException;

        public WarnigException(string message, string typeException = "999")
            : base(message)
        {
            _typeException = typeException;
        }

        public string GetTypeException()
        {
            return _typeException;
        }
    }

    public class MyXPCollection : XPCollection, ITypedList
    {
        public MyXPCollection() : base() { }
        public MyXPCollection(Session session, Type type) : base(session, type) { }
        public MyXPCollection(Session session, XPClassInfo type, IEnumerable list) : base(session, type, list) { }

        #region ITypedList impl
        internal abstract class ItemProperties
        {
            public Hashtable dispMembers;
            public XPPropertyDescriptorCollection displayProps;
            public readonly IXPClassInfoAndSessionProvider Context;
            public abstract string GetDisplayableProperties();
            public ItemProperties(IXPClassInfoAndSessionProvider context)
            {
                this.Context = context;
            }
        }
        class CollectionItemProperties : ItemProperties
        {
            public CollectionItemProperties(XPBaseCollection context) : base(context) { }
            public override string GetDisplayableProperties()
            {
                return ((XPBaseCollection)Context).DisplayableProperties;
            }
        }
        ItemProperties itemProperties;
        PropertyDescriptorCollection ITypedList.GetItemProperties(PropertyDescriptor[] listAccessors)
        {
            if (itemProperties == null)
                itemProperties = new CollectionItemProperties(this);
            return GetItemProperties(itemProperties, listAccessors);
        }
        internal static PropertyDescriptorCollection GetItemProperties(ItemProperties props, PropertyDescriptor[] listAccessors)
        {
            XPClassInfo info = GetMemberType(props.Context, listAccessors);
            if (info != null)
            {
                //if (props.Context.Session.IsDesignMode && listAccessors.Length > 4)
                //    return new PropertyDescriptorCollection(new PropertyDescriptor[] { });
                return GetDescriptorCollection(props, GetMember(props.Context, listAccessors[listAccessors.Length - 1]), info);
            }
            else
            {
                if ((listAccessors != null && listAccessors.Length != 0) || props.Context.ClassInfo == null)
                    return listAccessors[0].GetChildProperties();
                //return new PropertyDescriptorCollection(new PropertyDescriptor[] { });
                if (props.displayProps == null)
                {
                    props.displayProps = new XPPropertyDescriptorCollection(props.Context.Session, props.Context.ClassInfo, new PropertyDescriptor[] { });
                    foreach (string propertyName in props.GetDisplayableProperties().Split(';'))
                    {
                        if (propertyName.Length != 0)
                            props.displayProps.Find(propertyName, true);
                    }
                }
                return props.displayProps;
            }
        }
        static XPPropertyDescriptorCollection GetDescriptorCollection(ItemProperties props, XPMemberInfo mem, XPClassInfo info)
        {
            if (props.dispMembers == null)
                props.dispMembers = new Hashtable();
            XPPropertyDescriptorCollection coll = (XPPropertyDescriptorCollection)props.dispMembers[mem];
            if (coll == null)
            {
                coll = new XPPropertyDescriptorCollection(props.Context.Session, info, new PropertyDescriptor[] { });
                string refMember = string.Empty;
                if (mem.IsCollection)
                {
                    try
                    {
                        refMember = !mem.IsManyToMany ? mem.GetAssociatedMember().Name : string.Empty;
                    }
                    catch (Exception)
                    {
                        refMember = mem.Name;
                    }
                }
                System.Collections.Specialized.StringCollection dispProps = GetDefaultDisplayableProperties(info);
                foreach (string property in dispProps)
                {
                    if (property.Length != 0 && refMember != property)
                        coll.Find(property, true);
                }
                props.dispMembers[mem] = coll;
            }
            return coll;
        }
        static XPMemberInfo GetMember(IXPDictionaryProvider dictionary, PropertyDescriptor prop)
        {
            string name = XPPropertyDescriptor.GetMemberName(prop.Name);
            XPClassInfo inf = dictionary.Dictionary.GetClassInfo(prop.ComponentType);
            inf.GetMember(name);
            return MemberInfoCollection.FindMember(dictionary.Dictionary.GetClassInfo(prop.ComponentType), name);
        }
        static XPClassInfo GetMemberType(IXPDictionaryProvider dictionary, PropertyDescriptor[] listAccessors)
        {
            if (listAccessors != null && listAccessors.Length != 0 && typeof(XPBaseCollection).IsAssignableFrom(listAccessors[listAccessors.Length - 1].PropertyType))
            {
                XPMemberInfo mi = GetMember(dictionary, listAccessors[listAccessors.Length - 1]);
                if (mi.IsCollection)
                    return mi.CollectionElementType;
                else
                    return mi.ReferenceType;
            }
            return null;
        }
        string ITypedList.GetListName(PropertyDescriptor[] listAccessors)
        {
            return GetListName(listAccessors);
        }
        internal static string GetListName(PropertyDescriptor[] listAccessors)
        {
            if (listAccessors == null)
                return string.Empty;
            StringCollection coll = new StringCollection();
            foreach (PropertyDescriptor pd in listAccessors)
                coll.Add(pd.Name);
            return StringListHelper.DelimitedText(coll, ".");
        }
        #endregion

    }
}
