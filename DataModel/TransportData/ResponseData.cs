﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.TransportData
{
    public class ResponseData : RequestData
    {
        public string Status { get; set; }
        public string ErrorString { get; set; }
        public string FullErrorString { get; set; }
        public string ErrorCode { get; set; }
        public string Type { get; set; }

        public ResponseData()
        {
            Version = string.Empty;
            Status = "ERROR";
            ErrorString = string.Empty;
            ErrorCode = "000";
            Type = string.Empty;
        }
    }

    public class RequestData
    {
        public string Version { get; set; }
        public string Command { get; set; }
        public string Controller { get; set; }
        public string NameClassParams { get; set; }
        public object ClassParams { get; set; }
        public string Token { get; set; }
        public string UserId { get; set; }
        public string SerialHdd { get; set; }
        public string SerialOs { get; set; }
        public string SerialUsb { get; set; }
        public string HostClient { get; set; }
        public string Cripto { get; set; }
        public string UserModelName { get; set; }

        public RequestData()
        {
            ClassParams = new object();
            Controller = string.Empty;
            NameClassParams = string.Empty;
            Command = string.Empty;
            Version = string.Empty;
            Token = string.Empty;
            UserId = string.Empty;
            HostClient = System.Net.Dns.GetHostName();
        }
    }
}
