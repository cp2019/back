﻿using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    public class AuthData
    {
        public string Login { get; set; }
        public string Passwd { get; set; }
    }
    public class TokenData
    {
        public string token { get; set; }
    }
    public class ExceptionJSON
    {
        public int ErrorCode { get; set; }
        public string TextError { get; set; }
    }
    public class SuccessJSON
    {
        public int Code { get; set; }
        public string Message { get; set; }
        public string CustomBody { get; set; }
    }
    public class PageSourceJSON
    {
        public string pageSource { get; set; }
    }




    [NonPersistent]
    [OptimisticLocking(true)]
    [Serializable]
    public class BaseXPLiteObject : XpSinhObject
    {

        #region Конструктор

        public BaseXPLiteObject(Session session)
            : base(session)
        {
        }

        public BaseXPLiteObject()
            : base(Session.DefaultSession)
        {
        }

        #endregion

        #region Переменные

        private DevExpress.Xpo.Metadata.Helpers.MemberInfoCollection _ChangedMembers;

        #endregion

        #region Свойства

        public DevExpress.Xpo.Metadata.Helpers.MemberInfoCollection ChangedMembers
        {

            get
            {

                if (_ChangedMembers == null)
                {

                    _ChangedMembers = new DevExpress.Xpo.Metadata.Helpers.MemberInfoCollection(ClassInfo);

                }

                return _ChangedMembers;

            }

        }

        public string NameController { get; set; }
        public string NameParametr { get; set; }

        #endregion

        #region Переопределяемые методы формы

        protected override void OnChanged(string propertyName, object oldValue, object newValue)
        {

            base.OnChanged(propertyName, oldValue, newValue);

            if (!this.IsLoading)
            {

                XPMemberInfo Member = ClassInfo.GetPersistentMember(propertyName);
                if (Member != null && !ChangedMembers.Contains(Member))
                {

                    ChangedMembers.Add(ClassInfo.GetMember(propertyName));

                }

            }

        }

        protected override void OnSaved()
        {
            base.OnSaved();
            ChangedMembers.Clear();
        }

        #endregion
    }
    [NonPersistent]
    public class XpSinhObject : XPLiteObject
    {
        private string _guidKey;
        public XpSinhObject(Session session) : base(session) { }
        public XpSinhObject() : base(Session.DefaultSession) { }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            GuidKey = Guid.NewGuid().ToString();
        }

        [NonPersistent, MainXml]
        public string GuidKey
        {
            get { return _guidKey; }
            set { SetPropertyValue("GuidKey", ref _guidKey, value); }
        }
    }

}
