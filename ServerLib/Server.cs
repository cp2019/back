﻿using DataModel;
using System;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Text;
using System.Threading;

namespace Hackaton2019server
{
    public delegate void CallBackFunction(IAsyncResult result);
    public delegate string ParseRequest(string inputText);
    public class Server
    {
        private HttpListener _listener;
        private HttpListener _pingListner;

        public Server() { }

        public bool Start()
        {
            bool res = true; //RunListenerRequestMain();

            for (int i = 0; i < 100; i++)
            {
                if (RunListenerRequestAut())
                {
                    res = true;
                    break;
                }
                res = false;
            }
            return res;
        }

        public bool Stop()
        {
            try
            {
                _listener.Stop();
                _pingListner.Stop();
                _pingListner = null;
                _listener = null;
                LoggerOld.Info("Сервер успешно остановлен", "internallog.txt");
                return true;
            }
            catch (Exception)
            {
                LoggerOld.Info("Не удалось остановить сервер", "internallog.txt");
                return false;
            }
        }
        private void AuthoriseListnerCallback(IAsyncResult result)
        {
            ParseRequest parse = ExecuteIdentMethod;
            CallBackFunction callback = AuthoriseListnerCallback;
            ListenerCallbackTestAsync(result, parse, callback);
        }
        delegate void AsyncResponse(HttpListenerContext context, ParseRequest parse);
        private void GetAsyncResponse(object state)
        {
            try
            {
                WaitCallbacObject waitCallbackObject = state as WaitCallbacObject;
                HttpListenerRequest request = waitCallbackObject.Context.Request;
                StreamReader input = new StreamReader(request.InputStream);
                var inputText = input.ReadToEnd();
                string responseString = string.Empty;
                try
                {
                    responseString = waitCallbackObject.Parse(inputText);
                }
                catch (Exception ex)
                {
                    responseString = ex.ToString();
                    //Написать в лог с разных потоков
                }

                HttpListenerResponse response = waitCallbackObject.Context.Response;
                byte[] buffer = Encoding.UTF8.GetBytes(responseString);
                Stream output = response.OutputStream;
                response.AddHeader("Content-Encoding", "gzip");
                byte[] pr = ZipByte(buffer);
                response.ContentLength64 = pr.Length;
                output.Write(pr, 0, pr.Length);
                output.Close();
            }
            catch
            {
                //Ошибка в обработке запроса
            }
        }
        private void ListenerCallbackTestAsync(IAsyncResult result, ParseRequest parse, CallBackFunction callBack)
        {

            HttpListener listener = (HttpListener)result.AsyncState;
            if (!listener.IsListening) return;

            HttpListenerContext context = listener.EndGetContext(result);
            WaitCallback waite = new System.Threading.WaitCallback(GetAsyncResponse);
            WaitCallbacObject waiteCallbackObject = new WaitCallbacObject() { Context = context, Parse = parse };
            ThreadPool.QueueUserWorkItem(waite, waiteCallbackObject);
            result = listener.BeginGetContext(new AsyncCallback(callBack), listener);
        }
        private string ExecuteIdentMethod(string inputText)
        {
            //тут парсить, в модель ответа, входящий текст запроса;
            try
            {
                //data.CreateClientApplicationModel();
                //var session = data.ValidateOpenSession(true);
                //if (session.ValidateSessionByToken())
                //    data.MainDataXml = XmlEditor.GetOkCommand();
                //else XmlEditor.GetErrorCommand("Сессия еще не открыта", "999");
                ////HelperLogger.CreteDirectoriasByLogs(data);
                //if (data.MainDataXml.Name.LocalName != "Error")
                //{
                //    data.DefaultStrategy();
                //    return XmlEditor.CreateBasePatternResponse(data, "NeedAuthorization");
                //}
                AuthData ad = new AuthData
                {
                    Login = "user",
                    Passwd = "1111"
                };

                return Newtonsoft.Json.JsonConvert.SerializeObject(ad, Newtonsoft.Json.Formatting.Indented);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        private bool RunListenerRequestAut()
        {
            string prefixAuth = String.Format("http://{0}:{1}/", "+", Settings.Port);
            try
            {
                _pingListner = new HttpListener();
                _pingListner.Prefixes.Add(prefixAuth);
                _pingListner.Start();
                _pingListner.BeginGetContext(AuthoriseListnerCallback, _pingListner);                
                LoggerOld.Info("Запущен прослушиватель запросов для авторизации по адресу " + prefixAuth, "internallog.txt");
                return true;
            }
            catch
            {
              
                LoggerOld.Info("WARNING: Невозможно запустить прослушиватель запросов  для авторизации по адресу " + prefixAuth, "internallog.txt");
                return false;
            }
        }
        private byte[] ZipByte(byte[] byteArray)
        {
            MemoryStream ms = new MemoryStream();
            GZipStream sw = new GZipStream(ms,
            CompressionMode.Compress);
            sw.Write(byteArray, 0, byteArray.Length);
            sw.Close();
            return ms.ToArray();
        }
    }
    public class WaitCallbacObject
    {
        public HttpListenerContext Context { get; set; }
        public ParseRequest Parse { get; set; }
        public WaitCallbacObject() { }

    }
    static class LoggerOld
    {
        public static void Info(string info, string filename)
        {

            info = DateTime.Now.ToString() + " " + info;
            StreamWriter sw;
            FileInfo fi = new FileInfo("logs\\" + filename);
            CreateFileIfNotExist(fi);
            bool flag = true;
            while (flag)
            {
                try
                {
                    sw = fi.AppendText();
                    sw.WriteLine(info);
                    sw.Close();
                    flag = false;
                }
                catch
                {
                    Console.WriteLine("");
                }

            }
            //file.WriteLine("Info: " + info);
            Action addString = new Action(delegate
            {

                Settings.LogTextBox.AppendText(info + "\n");
            });
            if (!Settings.LogTextBox.InvokeRequired)
                addString();
            else
                Settings.LogTextBox.Invoke(addString);
        }
        private static void CreateFileIfNotExist(FileInfo fi)
        {
            if (fi.Directory != null && !fi.Directory.Exists)
                fi.Directory.Create();
            if (!fi.Exists)
                using (fi.Create())
                {
                }
        }
        public static void Failure(string failedRequest, Exception result)
        {
            StreamWriter sw;
            FileInfo fi = new FileInfo("logs\\crushes.txt");
            //fi.Create();
            bool flag = true;
            while (flag)
            {
                try
                {
                    sw = fi.AppendText();
                    sw.WriteLine("Error in external library occured");
                    sw.WriteLine("Date" + DateTime.Now.ToString("d-M-y_hh:mm:ss"));
                    sw.WriteLine("Request:");
                    sw.WriteLine(failedRequest);
                    sw.WriteLine("Exception:");
                    sw.WriteLine(result.Message);
                    sw.WriteLine(result.Source);
                    sw.WriteLine(result.StackTrace);
                    sw.Close();
                    flag = false;
                }
                catch { }
            }
            Action addString = new Action(delegate
            {

                Settings.LogTextBox.AppendText("Произошла ошибка при обращении к внешней библиотеке. Отчет об ошибке сформирован." + "\n");
            });
            if (!Settings.LogTextBox.InvokeRequired)
                addString();
            else
                Settings.LogTextBox.Invoke(addString);
        }

    }



}
