﻿using DataModel;
using DataModel.AdminData;
using Hackaton2019server.Controllers;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Text;
using System.Threading;
using System.Web.Script.Serialization;
using System.Collections.Specialized;

namespace Hackaton2019server
{
    public delegate void CallBackFunction(IAsyncResult result);
    public delegate string ParseRequestGet(string inputText, NameValueCollection headers);
    public delegate string ParseRequestPost(string body, string header, NameValueCollection headers);
    public class Server
    {
        private HttpListener _listener;
        private HttpListener _pingListner;

        public Server() { }

        public bool Start()
        {
            bool res = true; //RunListenerRequestMain();

            for (int i = 0; i < 100; i++)
            {
                if (RunListenerRequestAut())
                {
                    res = true;
                    break;
                }
                res = false;
            }
            return res;
        }

        public bool Stop()
        {
            try
            {
                _listener.Stop();
                _pingListner.Stop();
                _pingListner = null;
                _listener = null;
                LoggerOld.Info("Сервер успешно остановлен", "internallog.txt");
                return true;
            }
            catch (Exception)
            {
                LoggerOld.Info("Не удалось остановить сервер", "internallog.txt");
                return false;
            }
        }
        private void AuthoriseListnerCallback(IAsyncResult result)
        {
            ParseRequestGet parseGet = ExecuteIdentMethodGet;
            ParseRequestPost parsePost = ExecuteIdentMethodPost;
            CallBackFunction callback = AuthoriseListnerCallback;
            ListenerCallbackTestAsync(result, parseGet, parsePost, callback);
        }
        delegate void AsyncResponse(HttpListenerContext context, ParseRequestGet parse);
        private void GetAsyncResponse(object state)
        {
            try
            {
                WaitCallbacObject waitCallbackObject = state as WaitCallbacObject;
                HttpListenerRequest request = waitCallbackObject.Context.Request;
                StreamReader input = new StreamReader(request.InputStream);

                 var inputText = input.ReadToEnd();
                string responseString = string.Empty;
                try
                {
                    if(request.HttpMethod == "GET")
                        responseString = waitCallbackObject.ParseRequestGet(request.RawUrl, request.Headers);
                    if(request.HttpMethod == "POST")
                        responseString = waitCallbackObject.ParseRequestPost(inputText, request.RawUrl, request.Headers);                         
                }
                catch (Exception ex)
                {
                    responseString = ex.ToString();
                    //Написать в лог с разных потоков
                }

                HttpListenerResponse response = waitCallbackObject.Context.Response;
                
                if (request.HttpMethod == "OPTIONS")
                {
                    response.AddHeader("Access-Control-Allow-Headers", "Content-Type, Accept, token, X-Requested-With");
                    response.AddHeader("Access-Control-Allow-Methods", "GET, POST");
                    response.AddHeader("Access-Control-Max-Age", "1728000");
                }

                if (responseString == "401")
                    response.StatusCode = 401;

                response.ContentType = "application/json; charset=utf-8";
                response.AppendHeader("Access-Control-Allow-Origin", "*");

                byte[] buffer = Encoding.UTF8.GetBytes(responseString);
                Stream output = response.OutputStream;
                response.AddHeader("Content-Encoding", "gzip");
                byte[] pr = ZipByte(buffer);
                response.ContentLength64 = pr.Length;
                output.Write(pr, 0, pr.Length);
                output.Close();
            }
            catch
            {
                //Ошибка в обработке запроса
            }
        }
        private void ListenerCallbackTestAsync(IAsyncResult result, ParseRequestGet parseGet, ParseRequestPost parsePost, CallBackFunction callBack)
        {

            HttpListener listener = (HttpListener)result.AsyncState;
            if (!listener.IsListening) return;

            HttpListenerContext context = listener.EndGetContext(result);
            WaitCallback waite = new System.Threading.WaitCallback(GetAsyncResponse);
            WaitCallbacObject waiteCallbackObject = new WaitCallbacObject() { Context = context, ParseRequestGet = parseGet, ParseRequestPost = parsePost };
            ThreadPool.QueueUserWorkItem(waite, waiteCallbackObject);
            result = listener.BeginGetContext(new AsyncCallback(callBack), listener);
        }
        private string ExecuteIdentMethodGet(string inputText, NameValueCollection headers)
        {
            //тут парсить, в модель ответа, входящий текст запроса;
            try
            {

                if (inputText.Contains("?"))
                {
                    string res = string.Empty;
                    string[] vs = inputText.Split('?');
                    if (vs[0] == null)
                    {
                        //не корректный запрос
                        return string.Empty;
                    }
                        
                    if(vs[0] == "/OpenUrl")
                    {
                        OpenUrlController openUrlController = new OpenUrlController();
                        res = openUrlController.GetPageSource(vs[1]);
                        if (string.IsNullOrEmpty(res))
                            return "500";
                        return res;
                    }
                    if (vs[0] == "/Auth")
                    {
                        AuthorizationController authorizationController = new AuthorizationController();
                        res = authorizationController.GetToken(inputText);
                        if (res == string.Empty)
                        {
                            return "404";
                        }
                        else if (res == "401")
                            return res;
                        else
                        {
                            TokenData tokenData = new TokenData { token = res };
                            return Newtonsoft.Json.JsonConvert.SerializeObject(tokenData, Newtonsoft.Json.Formatting.Indented);
                        }
                    }
                }
                else if(inputText.Contains("/"))
                {
                    #region Односложные Get
                    if (inputText == "/Types")
                    {
                        TypesController typesController = new TypesController();
                        string res = typesController.GetTypesElements();
                        if (res == string.Empty)
                        {
                            return "404";
                        }
                        else if (res == "401")
                            return res;
                        else
                        {
                            return res;
                        }
                    }
                    if (inputText == "/Templates")
                    {
                        TemplateController templateController = new TemplateController();
                        string res = templateController.GetAllTemplates();
                        if (res == string.Empty)
                        {
                            return "404";
                        }
                        else if (res == "401")
                            return res;
                        else
                        {
                            return res;
                        }
                    }
                    #endregion
                    #region Двусложные get
                    string[] command = inputText.Split('/');
                    if (command == null)
                        return "500";
                    if(command.Length == 3)
                    {
                        if(command[1]=="Templates")
                        {
                            TemplateController templateController = new TemplateController();
                            int id;
                            if(int.TryParse(command[2], out id))
                             return templateController.GetTemplateByID(id);
                        }
                    }
                    #endregion

                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        private string ExecuteIdentMethodPost(string body, string header, NameValueCollection headers)
        {
            //тут парсить, в модель ответа, входящий текст запроса;
            try
            {

                //if (body.Contains("?"))
                //{
                //    string[] vs = body.Split('?');
                //    if (vs != null && vs[0] == "/Auth")
                //    {
                //        AuthorizationController authorizationController = new AuthorizationController();
                //        string res = authorizationController.GetToken(body);
                //        if (res == string.Empty)
                //        {
                //            return "404";
                //        }
                //        else if (res == "401")
                //            return res;
                //        else
                //        {
                //            TokenData tokenData = new TokenData { token = res };
                //            return Newtonsoft.Json.JsonConvert.SerializeObject(tokenData, Newtonsoft.Json.Formatting.Indented);
                //        }
                //    }
                //}
                if (header.Contains("/"))
                {
                    if(header == "/Templates")
                    {
                        TemplateController templateController = new TemplateController();
                        templateController.CreateTemplate(body);
                    }
                    if (body == "/Users")
                    {
                        UserController usersController = new UserController();
                        string res = usersController.GetUsers(body);
                        if (res == string.Empty)
                        {
                            return "404";
                        }
                        else if (res == "401")
                            return res;
                        else
                        {
                            return res;
                        }
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        private bool RunListenerRequestAut()
        {
            string prefixAuth = String.Format("http://{0}:{1}/", "+", Settings.Port);
            try
            {
                _pingListner = new HttpListener();
                _pingListner.Prefixes.Add(prefixAuth);
                _pingListner.Start();
                _pingListner.BeginGetContext(AuthoriseListnerCallback, _pingListner);                
                LoggerOld.Info("Запущен прослушиватель запросов для авторизации по адресу " + prefixAuth, "internallog.txt");
                return true;
            }
            catch
            {
              
                LoggerOld.Info("WARNING: Невозможно запустить прослушиватель запросов  для авторизации по адресу " + prefixAuth, "internallog.txt");
                return false;
            }
        }
        private byte[] ZipByte(byte[] byteArray)
        {
            MemoryStream ms = new MemoryStream();
            GZipStream sw = new GZipStream(ms,
            CompressionMode.Compress);
            sw.Write(byteArray, 0, byteArray.Length);
            sw.Close();
            return ms.ToArray();
        }
    }
    public class WaitCallbacObject
    {
        public HttpListenerContext Context { get; set; }
        public ParseRequestGet ParseRequestGet { get; set; }
        public ParseRequestPost ParseRequestPost { get; set; }
        public WaitCallbacObject() { }

    }
    static class LoggerOld
    {
        public static void Info(string info, string filename)
        {

            info = DateTime.Now.ToString() + " " + info;
            StreamWriter sw;
            FileInfo fi = new FileInfo("logs\\" + filename);
            CreateFileIfNotExist(fi);
            bool flag = true;
            while (flag)
            {
                try
                {
                    sw = fi.AppendText();
                    sw.WriteLine(info);
                    sw.Close();
                    flag = false;
                }
                catch
                {
                    Console.WriteLine("");
                }

            }
            //file.WriteLine("Info: " + info);
            Action addString = new Action(delegate
            {

                Settings.LogTextBox.AppendText(info + "\n");
            });
            if (!Settings.LogTextBox.InvokeRequired)
                addString();
            else
                Settings.LogTextBox.Invoke(addString);
        }
        private static void CreateFileIfNotExist(FileInfo fi)
        {
            if (fi.Directory != null && !fi.Directory.Exists)
                fi.Directory.Create();
            if (!fi.Exists)
                using (fi.Create())
                {
                }
        }
        public static void Failure(string failedRequest, Exception result)
        {
            StreamWriter sw;
            FileInfo fi = new FileInfo("logs\\crushes.txt");
            //fi.Create();
            bool flag = true;
            while (flag)
            {
                try
                {
                    sw = fi.AppendText();
                    sw.WriteLine("Error in external library occured");
                    sw.WriteLine("Date" + DateTime.Now.ToString("d-M-y_hh:mm:ss"));
                    sw.WriteLine("Request:");
                    sw.WriteLine(failedRequest);
                    sw.WriteLine("Exception:");
                    sw.WriteLine(result.Message);
                    sw.WriteLine(result.Source);
                    sw.WriteLine(result.StackTrace);
                    sw.Close();
                    flag = false;
                }
                catch { }
            }
            Action addString = new Action(delegate
            {

                Settings.LogTextBox.AppendText("Произошла ошибка при обращении к внешней библиотеке. Отчет об ошибке сформирован." + "\n");
            });
            if (!Settings.LogTextBox.InvokeRequired)
                addString();
            else
                Settings.LogTextBox.Invoke(addString);
        }

    }



}
