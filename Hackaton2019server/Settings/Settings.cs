﻿using System.ComponentModel;
using System.Windows.Forms;
using System.Xml.Linq;

namespace Hackaton2019server
{
    public class Settings
    {
        public static RichTextBox LogTextBox { get; set; }
        public static string Port { get; set; }
        public static string ServerName { get; set; }
        [Description("Порт для подключения к базе данных MySql")]
        public static string PortMySql { get; set; }
        [Description("Имя базы данных")]
        public static string DataBase { get; set; }
        [Description("Имя пользователя для соединения с БД")]
        public static string UserNameMySql { get { return "Monica"; } }
        [Description("Пароль пользователя ")]
        public static string Passward { get { return "MonicaM5"; } }
        [Description("Хост сервера для связи из клиента")]
        public static string ServerHost { get; set; }
        public static string ListDataBase { get; set; }
        [Description("Конечный хост для соединения")]
        public static string HostConnect
        {
            get { return string.Format("http://{0}:{1}/", ServerHost, Port); }
        }

    }
    public class SettingsTools
    {
        public static void LoadSettingsFromXml(XDocument document)
        {
            if (document.Root == null)
                return;
            XElement elementMySql = document.Root.Element("MySql");
            XElement elementService = document.Root.Element("Service");
            XElement elementClient = document.Root.Element("Client");
            XElement elementUpdate = document.Root.Element("Update");
            XElement elementCryptoProvider = document.Root.Element("CryptoProvider");

            foreach (XElement element in elementMySql.Elements("Parametr"))
            {
                if (element.Attribute("Name").Value.Equals("Server"))
                    Settings.ServerName = element.Attribute("Value").Value;
                if (element.Attribute("Name").Value.Equals("Port"))
                    Settings.PortMySql = element.Attribute("Value").Value;
                if (element.Attribute("Name").Value.Equals("DataBase"))
                    Settings.DataBase = element.Attribute("Value").Value;
                if (element.Attribute("Name").Value.Equals("ListBd"))
                    Settings.ListDataBase = element.Attribute("Value").Value;
            }

            foreach (XElement element in elementService.Elements("Parametr"))
            {
                if (element.Attribute("Name").Value.Equals("Port1"))
                    Settings.Port = element.Attribute("Value").Value;
            }
        }
    }

}

