﻿using DataModel.AdminData;
using Hackaton2019server.Controllers;
using System;
using System.Windows.Forms;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;

namespace Hackaton2019server.Forms
{
    public partial class MainForm : Form
    {
        private Server _server;
        public MainForm()
        {
            InitializeComponent();
            btnSettings.Click += BtnSettingOnClick;
            _server = new Server();
            Settings.LogTextBox = this.tbLog;
            StartServer();
            //Program.Test();
        }
        private void BtnSettingOnClick(object sender, EventArgs eventArgs)
        {
            SettingsForm setForm = new SettingsForm();
            setForm.ShowDialog();
        }
        private void StartServer()
        {
            if (string.IsNullOrWhiteSpace(Settings.ServerName) || string.IsNullOrWhiteSpace(Settings.Port)
               || string.IsNullOrWhiteSpace(Settings.PortMySql))
            {
                MessageBox.Show("Нет настроек сервера! Настройте пожалуйста сервер!");
                return;
            }
            _server.Start();
        }
        

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            //Добавим юзера
            MainController mainController = new MainController();
            h_user h_User = new h_user(mainController.MySession);
            h_User.Login = "Roberto2";
            h_User.Passwd = "qwerty";
            mainController.MySession.CommitChanges();
        }
    }
}
