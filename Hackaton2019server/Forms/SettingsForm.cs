﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace Hackaton2019server.Forms
{
    public partial class SettingsForm : Form
    {
        public SettingsForm()
        {
            InitializeComponent();
            btnOk.Click += BtnOkOnClick;
            btnExit.Click += BtnExitOnClick;

            txtServerName.Text = Settings.ServerName;
            txtPort.Text = Settings.PortMySql;
            txtPortService.Text = Settings.Port;
            txtListBd.Text = Settings.ListDataBase;
        }
        private void BtnExitOnClick(object sender, EventArgs eventArgs)
        {
            Close();
        }

        private void BtnOkOnClick(object sender, EventArgs eventArgs)
        {
            SaveToXml();
            Close();
        }
        #region Методы работы с файлом настрйки

        public void SaveToXml()
        {
            XDocument document = new XDocument();
            var elementRoot = new XElement("Settings");
            var elementMySql = new XElement("MySql",
                new XElement("Parametr", new XAttribute("Name", "Server"), new XAttribute("Value", txtServerName.Text)),
                new XElement("Parametr", new XAttribute("Name", "Port"), new XAttribute("Value", txtPort.Text)),
                new XElement("Parametr", new XAttribute("Name", "ListBd"), new XAttribute("Value", txtListBd.Text)));
            var elementService = new XElement("Service",
                new XElement("Parametr", new XAttribute("Name", "Port1"), new XAttribute("Value", txtPortService.Text)));
            elementRoot.Add(elementMySql, elementService);
            document.Add(elementRoot);
            document.Save(Application.StartupPath + "\\server.set");
            if (File.Exists(Application.StartupPath + "\\server.set"))
                SettingsTools.LoadSettingsFromXml(XDocument.Load(Application.StartupPath + "\\server.set"));
        }

        #endregion
    }
}
