﻿using DataModel.AdminData;
using DevExpress.Data.Filtering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hackaton2019server.Controllers
{
    public class AuthorizationController : MainController
    {
        public virtual string GetToken(string input)
        {
            string login =string.Empty;
            string password = string.Empty;
            input = input.Substring(6);
            string[] tmpStr = input.Split('&');
            if(tmpStr[0]!=null && tmpStr[0].Contains("login")&&tmpStr[0].Contains("="))
            {
                login = tmpStr[0].Substring(6);
            }
            if(tmpStr[1]!=null && tmpStr[1].Contains("password") && tmpStr[1].Contains("="))
            {
                password = tmpStr[1].Substring(9);
            }



            h_user targetUser = MySession.FindObject<h_user>(CriteriaOperator.Parse("Login = ? and Passwd = ?", login, password));
            if(targetUser!=null)
            {
                targetUser.Token = Guid.NewGuid().ToString();
            }
            else
            {
                return "401";
            }
            MySession.CommitChanges();
            return targetUser.Token;
        }
        public override void ParseGetRequest(string inputText)
        {
            
        }
        
    }
}
