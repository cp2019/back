﻿using DataModel.AdminData;
using DataModel.DTO;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using Hackaton2019server.HelpersTools;
using Hackaton2019server.SeleniumEngine;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Hackaton2019server.Controllers
{
    public class TemplateController : MainController
    {
        public virtual void CreateTemplate(string body)
        {
            TemplateDTODesirialize templateDTODesirialize = ParseAPI.ParsePostTemplate(body);
            h_template template = new h_template(MySession);
            template.Name = templateDTODesirialize.name;
            
            foreach(var item in templateDTODesirialize.elements)
            {
                h_elemnet elemnet = new h_elemnet(MySession);
                elemnet.Text = item.text;
                elemnet.Template = template;
                elemnet.Id = item.id;
                elemnet.Step = item.step;
                h_elementType elementType = MySession.FindObject<h_elementType>(CriteriaOperator.Parse("Sysid = ?", item.type));
                if (elementType != null)
                    elemnet.ElementType = elementType;
            }
            MySession.CommitChanges();

            //IWebDriver webDriver = SeleniumCore.Instanse().IwebDriver;
            //url = url.Split('=')[1];
            //webDriver.Url = url;
            //webDriver.Navigate();
            //PageSourceJSON pageSource = new PageSourceJSON { pageSource = webDriver.PageSource };
            //return Newtonsoft.Json.JsonConvert.SerializeObject(pageSource, Newtonsoft.Json.Formatting.Indented);
        }
        public virtual string GetAllTemplates()
        {
            var jsSerializator = new JavaScriptSerializer();
            XPCollection<h_template> templates = new XPCollection<h_template>(MySession);
            List<TemplateDTO> result = new List<TemplateDTO>();
            foreach (var item in templates)
            {
                result.Add(item.GetTemplateDTO(item));
            }
            //jsSerializator.RecursionLimit = 1;
            return jsSerializator.Serialize(result);
        }
        public virtual string GetTemplateByID(int id)
        {
            h_template template = MySession.FindObject<h_template>(CriteriaOperator.Parse("Sysid = ?", id));
            if (template == null)
                return string.Empty;
            TemplateDTO templateDTO = template.GetTemplateDTO(template);
            var jsSerializator = new JavaScriptSerializer();
            return jsSerializator.Serialize(templateDTO);
        }
    }
}
