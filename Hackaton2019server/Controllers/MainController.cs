﻿using DataModel.AdminData;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hackaton2019server.Controllers
{
    public class MainController
    {
        [Description("Рабочая сессия")]
        public Session _session { get; set; }
        public UnitOfWork MySession { get; set; }

        public MainController()
        {
            var mySqlConnection = new MySqlConnection(
               string.Format(
                   "server={0};port={1};user id={2}; password={3}; Connect Timeout={4}; charset=utf8; DataBase = {5}; Allow Zero Datetime=true; Pooling=false",
                   Settings.ServerName, Settings.PortMySql, Settings.UserNameMySql, Settings.Passward,
                   "3000", Settings.ListDataBase));
            _session = new Session();
            _session.Connection = mySqlConnection;
            MySession = new UnitOfWork(_session.DataLayer);

        }
        public virtual void InputDatabase()
        {

            h_task h_Task = new h_task(MySession);
            h_Task.Description = "testTask";
            h_Task.Name = "Test";
            h_Task.Templates.Add(MySession.FindObject<h_template>(CriteriaOperator.Parse("Sysid = ?", 1)));
            MySession.CommitChanges();

            //h_elementType elementType1 = new h_elementType(MySession);
            //elementType1.Name = "text";
            //elementType1.Discription = string.Format("Вставьте текст", Encoding.UTF8);

            //h_elementType elementType2 = new h_elementType(MySession);
            //elementType2.Name = "checkbox";
            //elementType2.Discription = string.Format("Поставить галочку", Encoding.UTF8); 

            //h_elementType elementType3 = new h_elementType(MySession);
            //elementType3.Name = "buttone";
            //elementType3.Discription = string.Format("Кликнуть по кнопке", Encoding.UTF8);

            //MySession.CommitChanges();



        }
        public virtual void ParseGetRequest(string inputText)
        {

        }

    }
}
