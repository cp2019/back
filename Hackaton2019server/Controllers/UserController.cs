﻿using DataModel;
using DataModel.AdminData;
using DataModel.DTO;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Hackaton2019server.Controllers
{
    public class UserController : MainController
    {
        public virtual string GetUsers(string input)
        {

            var jsSerializator = new JavaScriptSerializer();
            XPCollection<h_user> users = new XPCollection<h_user>(MySession);
            List<UserDTO> result = new List<UserDTO>();
            foreach(var item in users)
            {
                result.Add(item.GetUserDTO());
            }            
            //jsSerializator.RecursionLimit = 1;
            return jsSerializator.Serialize(result);
        }
        public virtual string AddUser(string body)
        {
            try
            {
                string result = string.Empty;
                
                //  ParseAPI.

                return result;
            }
            catch(Exception ex)
            {
                ExceptionJSON tokenData = new ExceptionJSON {ErrorCode = 500, TextError= ex.Message};
                return Newtonsoft.Json.JsonConvert.SerializeObject(tokenData, Newtonsoft.Json.Formatting.Indented);
            }
        }
    }

}
