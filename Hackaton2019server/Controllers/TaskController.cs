﻿using DataModel.AdminData;
using Hackaton2019server.SeleniumEngine;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hackaton2019server.Controllers
{
    public class TaskController : MainController
    {
        public virtual void CreateTask(h_task h_Task)
        {
            SeleniumTokenCore seleniumTokenCore = new SeleniumTokenCore();
            IWebDriver webDriver = seleniumTokenCore.CreateInstanseByToken(h_Task.SessionTask);
            Engine engine = new Engine(webDriver);
            engine.RunTask(h_Task);
        }
    }
}
