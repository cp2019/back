﻿using DataModel;
using Hackaton2019server.SeleniumEngine;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hackaton2019server.Controllers
{
    public class OpenUrlController : MainController
    {
        public virtual string GetPageSource(string url)
        {
            IWebDriver webDriver = SeleniumCore.Instanse().IwebDriver;
            url = url.Split('=')[1];
            webDriver.Url = url;
            webDriver.Navigate();
            PageSourceJSON pageSource = new PageSourceJSON { pageSource = webDriver.PageSource };
            return Newtonsoft.Json.JsonConvert.SerializeObject(pageSource, Newtonsoft.Json.Formatting.Indented);
        }
    }
}
