﻿using DataModel.AdminData;
using DataModel.DTO;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Hackaton2019server.Controllers
{
    public class TypesController : MainController
    {
        public string GetTypesElements()
        {

            var jsSerializator = new JavaScriptSerializer();
            XPCollection<h_elementType> elementsType = new XPCollection<h_elementType>(MySession);
            List<ElementTypeDTO> result = new List<ElementTypeDTO>();
            foreach (var item in elementsType)
            {
                result.Add(item.GetFileTypeDTO());
            }
            //jsSerializator.RecursionLimit = 1;
            return jsSerializator.Serialize(result);
        }
    }
}
