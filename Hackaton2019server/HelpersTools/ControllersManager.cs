﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hackaton2019server.HelpersTools
{
    public class ControllersManager
    {
        public static object GetControlByHeader(string controllerName)
        {
            Type t = Type.GetType("Hackaton2019server.Controllers." + controllerName);
            return Activator.CreateInstance(t);
        }
    }
}
