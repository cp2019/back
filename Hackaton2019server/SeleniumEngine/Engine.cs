﻿using DataModel.AdminData;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hackaton2019server.SeleniumEngine
{
    public interface IEngine
    {
        void RunTask(h_task task);
        void RunTemplate(h_template template);
    }
    public interface IElementRun
    {
        void Action();
    }


    public class Engine: IEngine
    {
        private IWebDriver _webDriver;
        public Engine(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }
        public virtual void RunTask(h_task task)
        {
            foreach(var item in task.Templates)
            {
                RunTemplate(item);
            }
        }
        public virtual void RunTemplate(h_template template)
        {
            foreach(var item in template.Elements.OrderBy(f=>f.Step))
            {
                ElemntAction elemntAction = new ElemntAction(_webDriver, item);
                elemntAction.Action();
            }
        }

        
    }
}
