﻿using DataModel.AdminData;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hackaton2019server.SeleniumEngine
{
    public class ElemntAction : IElementRun
    {
        private h_elemnet _elemnet;
        private IWebDriver _webDriver;
        public ElemntAction(IWebDriver webDriver, h_elemnet element)
        {
            _elemnet = element;
            _webDriver = webDriver;
        }
        public void Action()
        {
            IWebElement webElement = null;
            if (_elemnet.ElementType.Name == "text")
            {
                webElement = _webDriver.FindElement(By.CssSelector(_elemnet.Id));
                if(webElement!=null)
                 webElement.SendKeys(_elemnet.Text);
            }
            if(_elemnet.ElementType.Name == "button")
            {
                webElement = _webDriver.FindElement(By.CssSelector(_elemnet.Id));
                if (webElement != null)
                    webElement.Click();
            }
            if (_elemnet.ElementType.Name == "checkbox")
            {
                webElement = _webDriver.FindElement(By.CssSelector(_elemnet.Id));
                if (webElement != null)
                    webElement.Click();
            }
            if (_elemnet.ElementType.Name == "url")
            {
                _webDriver.Url = _elemnet.Text;
                _webDriver.Navigate();
            }
        }
    }
}
