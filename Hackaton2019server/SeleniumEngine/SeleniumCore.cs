﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hackaton2019server.SeleniumEngine
{
    public class SeleniumCore
    {
        private static SeleniumCore instance;
        public IWebDriver IwebDriver { get; private set; }
        private SeleniumCore()
        {
            IwebDriver = new ChromeDriver();
        }
        public static SeleniumCore Instanse()
        {
            if (instance == null)
            {
                instance = new SeleniumCore();
                return instance;
            }
            else
                return instance;                
        }        
    }
    public class SeleniumTokenCore
    {
        private static Dictionary<string, IWebDriver> cashDrivers;
        public SeleniumTokenCore()
        {
            cashDrivers = new Dictionary<string, IWebDriver>();
        }
        public IWebDriver CreateInstanseByToken(string token)
        {
            try
            {
                if (cashDrivers.ContainsKey(token))
                {
                    IWebDriver webDriver = null;
                    cashDrivers.TryGetValue(token, out webDriver);
                    return webDriver;
                }
                else
                {
                    IWebDriver webDriver = new ChromeDriver();
                    cashDrivers.Add(token, webDriver);
                    return webDriver;
                }
            }
            catch
            {
                return SeleniumCore.Instanse().IwebDriver;
            }

        }
    }



}
